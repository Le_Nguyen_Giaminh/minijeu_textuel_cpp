#include <iostream>
#include <cstdlib>
#include <ctime>

void gameplay(int atkP, int defP, int emP, int spdP, int critP) {

    // player's stats
    int atk = atkP;
    int def = defP;
    int em = emP;
    int spd = spdP;
    int crit = critP;

    // varible to represent the player and the monster's choice of move
    int choice;

    srand((unsigned)time(0));
    // variable to decide if the player or the monster play first
    int init = rand()%2+1;

    // initialize player´s hp
    int player_hp = 100;

    // initialize monster hp
    int monster_hp = 100;

    // damage to monster and player each round
    int damage_monster;
    int damage_player;

    // game start
    std::cout<<"===== Let's play Nekorimu ! =====\n\n";
    if (init == 1) {
        std::cout<<"Your start first !\n==========\n";
        while (player_hp > 0 || monster_hp > 0) {
            std::cout<<"Choose your move (enter a number): \n1 - Normal Attack\n2 - Special Attack\n";
            do{std::cin>>choice;}
                while(choice>3 || choice<1);
                switch (choice) {
                    case 1:
                        damage_monster = rand()%10 + (atk+em+crit)/2;
                        break;
                    case 2:
                        damage_monster = rand()%20 + (atk+em+crit)/2;
                        break;
                }
                
                // monster's turn
                choice = rand()%2;
                switch (choice) {
                    case 1:
                        damage_player = rand()%20 + 1;
                        break;
                    case 2:
                        damage_player = rand()%60 + 21;
                        break;
                    }

                monster_hp = monster_hp - damage_monster;
                std::cout<<"You did "<< damage_monster <<" damage to the monster!\n";

                if (monster_hp < 1) {
                    std::cout<<"You killed the monster!! You won with " << player_hp << " hp left.\n";
                    std::cout<<"The end, bye bye";
                    break;
                }

                std::cout<<"The monster now have " << monster_hp << " hp left.\n";
                
                player_hp = player_hp - damage_player;
                std::cout<<"The monster hit you for " << damage_player << " damage.\n";

                if (player_hp < 1) {
                    std::cout<<"You died. The monster still has " << monster_hp << " hp left.\n";
                    std::cout<<"The end, bye bye";
                    break;
                }
                std::cout << "You now have " << player_hp << " hp left.\n";
                std::cout << "\n==========\n";
        }
    }

    else {
        std::cout<<"Monster starts first.\n==============\n";
            while (player_hp > 0 || monster_hp > 0) {
                choice = rand()%2;
                switch (choice) {
                    case 1:
                        damage_player = rand()%20 + 1;
                        break;
                    case 2:
                        damage_player = rand()%60 + 21;
                        break;
	            }

                player_hp = player_hp - damage_player;
                std::cout<<"The monster hit you for " << damage_player << " damage.\n";

                if (player_hp < 1) {
                    std::cout<<"You died. The monster still has " << monster_hp <<" hp left.\n";
                    std::cout<<"The end, bye bye";
                    break;
                }
                std::cout<<"You now have " << player_hp << " hp left.\n\n";

                // player's turn
                std::cout<<"Choose your move (enter a number): \n1 - Normal Attack\n2 - Special Attack\n";
                do{std::cin>>choice;}
                    while(choice>3 || choice<1);
                        switch (choice) {
                            case 1:
                                damage_monster = rand()%10 + (atk+em+crit)/2;
                            break;
                            case 2:
                                damage_monster = rand()%20 + (atk+em+crit)/2;
                            break;
                        }
                monster_hp = monster_hp - damage_monster;
                std::cout<<"You did "<< damage_monster <<" damage to the monster!\n";

                if (monster_hp < 1) {
                    std::cout<<"You killed the monster!! You won with " << player_hp << " hp left.\n";
                    std::cout<<"The end, bye bye";
                    break;
                }
                std::cout<<"The monster now have " << monster_hp << " hp left.\n";
                std::cout << "\n==========\n";
            } 
        } 
}