#include <iostream>
#include <string>
#include "../lib/tsl/ordered_map.h"

tsl::ordered_map<std::string, std::string> myCharacter;

// initialize each variable with a default value for the character
void init_character() {

    // name
    myCharacter.insert(std::make_pair("Name", "Diluc"));

	// appearance
	myCharacter.insert(std::make_pair("Gender", "neutral"));
	myCharacter.insert(std::make_pair("Hair", "black"));
    myCharacter.insert(std::make_pair("Eyes", "black"));
    myCharacter.insert(std::make_pair("Skin", "white"));
	myCharacter.insert(std::make_pair("Height", "medium"));

	// clothes
	myCharacter.insert(std::make_pair("Top", "black"));
	myCharacter.insert(std::make_pair("Bottom", "blue"));
    myCharacter.insert(std::make_pair("Shoes", "black"));

    // class
    myCharacter.insert(std::make_pair("Class", "knight"));

	// weapon
    myCharacter.insert(std::make_pair("Weapon", "sword"));

    // element
    myCharacter.insert(std::make_pair("Element", "fire"));

	// traits
	myCharacter.insert(std::make_pair("Trait 1", "funny"));
	myCharacter.insert(std::make_pair("Trait 2", "kind"));
	myCharacter.insert(std::make_pair("Trait 3", "talkative"));

	// stats 
	myCharacter.insert(std::make_pair("Attack", "20"));
	myCharacter.insert(std::make_pair("Defense", "20"));
	myCharacter.insert(std::make_pair("Elemental mastery", "20"));
	myCharacter.insert(std::make_pair("Speed", "20"));
	myCharacter.insert(std::make_pair("Crit rate", "20"));
}

// print the character description
void printCharacterDescription(tsl::ordered_map<std::string, std::string>const &myMap) {
    for (auto const &pair : myMap) {
        std::cout << pair.first << ": " << pair.second << std::endl;
    }
}

// character customization
void characterCustomize(tsl::ordered_map<std::string, std::string> &myMap) {
	std::string x;
	for (auto &pair : myMap) {
        std::cout << "Enter the value for " << pair.first << ": ";
        std::cin >> x;
		myMap[pair.first] = x;
    }
}