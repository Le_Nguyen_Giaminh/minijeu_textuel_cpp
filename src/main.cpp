#include <iostream>
#include <string>
#include "character.cpp"
#include "gameplay.cpp"
#include "../lib/tsl/ordered_map.h"

extern tsl::ordered_map<std::string, std::string> myCharacter;

int main(int argc, char *argv[])
{
	std::cout << "Welcome to Nekorimu - a text-based RPG" << std::endl;

	// initialize a character with default values
	init_character();
	// print the description of the character
	std::cout << "This is your default character, please customize it" << std::endl;
	printCharacterDescription(myCharacter);

	// ask user to customize the character
	characterCustomize(myCharacter);
	// print the descriptionn of the customized character 
	std::cout << "This is your customized character" << std::endl;
	printCharacterDescription(myCharacter);
	
	// start the game
	int atkP = std::stoi(myCharacter["Attack"]);
	int defP = std::stoi(myCharacter["Defense"]);
	int emP = std::stoi(myCharacter["Elemental mastery"]);
	int spdP = std::stoi(myCharacter["Speed"]);
	int critP = std::stoi(myCharacter["Crit rate"]);

	gameplay(atkP, defP, emP, spdP, critP);
	return 0;
} 