# ✨Nekorimu - A mini text-based RPG✨
## This project is created in a C++ crash course

Nekorimu is a text-based RPG that players get to attack monsters until they die (OR you die hehehe).

![alt text](https://i.pinimg.com/originals/32/64/91/326491f0c02cc7ba788e938f9718acae.gif)

## Run

Nekorimu requires [g++](https://gcc.gnu.org/) v7.5.0+ to run.

Compile and run the file "main" to play

```sh
g++ main.cpp -o main.out
./main.out
```

## Libaries
| Library | Link |
| ------ | ------ |
| ordered map | [https://github.com/Tessil/ordered-map] |

## License
MIT

**Free Software, hell yeah!**
